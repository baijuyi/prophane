from utils.config import INCONCLUSIVE_ANNOTATIONS
import pandas as pd
import gzip

def create_foam_map(dbobj, ont_in, map_out, missing_out):
    df = pd.read_csv(ont_in, sep='\t', names=['level1', 'level2', 'level3', 'level4', 'ko'], skiprows=1)
    with gzip.open(map_out, "wt") as map_handle, gzip.open(missing_out, "wt") as missing_handle:
        map_handle.write("acc\tlevel1\tlevel2\tlevel3\tlevel4\tko\tacc\n")
        for entry in dbobj.iter_hmmlib():
            ko = entry['NAME'].split(":")[1].split("_")[0]
            annots = df.loc[df.ko == ko]
            if len(annots.index) == 0:
                missing_handle.write(entry['ACC'] + "\n")
                continue
            for index, row in annots.iterrows():
                map_handle.write("\t".join([entry['ACC']] + [str(x) for x in row.values.tolist()] + [entry['ACC']]) + "\n")

rule make_foam_map:
    input:
        "{db_base_dir}/foam/{db_version}/{foamsdb}.tsv"
    output:
        "{db_base_dir}/foam/{db_version}/{foamsdb}.map.gz",
        "{db_base_dir}/foam/{db_version}/{foamsdb}.unmapped.log.gz"
    wildcard_constraints:
        fname = ".*/FOAM[^/]+"
    message:
        "generating map {output[0]}"
    benchmark:
        "{db_base_dir}/foam/{db_version}/{foamsdb}.map.gz.benchmark.txt"
    resources:
        mem_mb=400
    version:
        "0.1"
    params:
        db_obj=lambda wildcards: get_db_object_for_dbtype_and_version("foam", wildcards.db_version, wildcards.db_base_dir)
    run:
        create_foam_map(params.db_obj, input[0], output[0], output[1])
