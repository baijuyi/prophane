from utils.config import INCONCLUSIVE_ANNOTATIONS
import pandas as pd
import gzip

def create_resfam_map(tsv_in, map_out):
    df = pd.read_csv(tsv_in, sep='\t', usecols=['ResfamID', 'Resfam Family Name', 'Description', 'Mechanism Classification', 'Antibiotic Classification (Resfam Only)'])
    with gzip.open(map_out, "wt") as map_handle:
        map_handle.write("resfam id\tmechanistic class\tantibiotic class\tresfam family\tdescr\tresfam id\n")
        for index, row in df.iterrows():
                data = [str(x) for x in row.values.tolist()]
                map_handle.write("\t".join([data[0], data[3], data[4], data[1], data[2], data[0]]) + "\n")

rule make_resfam_map:
    input:
        "{db_base_dir}/resfams_{branch}/{db_version}/{resfamsdb}.tsv"
    output:
        "{db_base_dir}/resfams_{branch}/{db_version}/{resfamsdb}.map.gz",
    wildcard_constraints:
        fname = ".*/resfam_[^/]+"
    message:
        "generating map {output[0]}"
    benchmark:
        "{db_base_dir}/resfam_{branch}/{db_version}/{resfamsdb}.map.gz.benchmark.txt"
    resources:
        mem_mb=400
    version:
        "0.1"
    run:
        create_resfam_map(input[0], output[0])
