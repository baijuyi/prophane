def linked_db_files(wildcards):
    db_obj = get_db_object_for_dbtype_and_version("ncbi_nr", wildcards.db_version, wildcards.db_base_dir)
    taxdump_map = db_obj.get_tax2annot_map_file()
    lst_acc2tax_files = db_obj.get_list_of_acc2tax_files()
    return [taxdump_map] + lst_acc2tax_files


rule make_nr_acc2tax_map:
    input:
        linked_db_files
    output:
        "{db_base_dir}/ncbi_nr/{db_version}/{fname}.map.gz",
        "{db_base_dir}/ncbi_nr/{db_version}/{fname}.unmapped.log.gz"
    message:
        "generating taxmap {output[0]}"
    benchmark:
        "{db_base_dir}/ncbi_nr/{db_version}/{fname}.map.gz.benchmark.txt"
        # "{fname}.map.gz.benchmark.txt"
    resources:
        mem_mb=1024*3
    params:
        db_obj=lambda wildcards: get_db_object_for_dbtype_and_version("ncbi_nr", wildcards.db_version, wildcards.db_base_dir)
    run:
        params.db_obj.create_acc2tax_map(
            taxdump_in=input[0], l_acc2taxid_in=input[1:], nr_out=output[0], nr_missing_out=output[1])
