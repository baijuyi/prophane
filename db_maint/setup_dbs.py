#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import glob
import os
import argparse


def set_db_config_paths(db_basedir, test=False, reverse=False):
    if not reverse:
        mods = [
            ("{db_basedir}", db_basedir)
        ]
    else:
        mods = [
            (db_basedir, "{db_basedir}")
        ]
    yamls = replace_yaml_paths(db_basedir, mods, test)
    return yamls


def set_dir(db_basedir=os.path.dirname(os.path.abspath(__file__)), test=False, reverse=False):
    if reverse:
        print("REVERSE MODE")
    set_db_config_paths(db_basedir, test, reverse)


def replace_yaml_paths(basedir, mods, test=False):
    yamls = []
    for fname in glob.iglob(str(basedir) + "/**/*.yaml", recursive=True):
        yamls.append(fname)
        actions = 0
        with open(fname, "r") as handle:
            content = handle.read()
            for m, n in mods:
                actions += content.count(m)
                content = content.replace(m, n)
        if not test:
            with open(fname, "w") as handle:
                handle.write(content)
        print(fname, actions, "actions")
    return yamls


def parse_args():
    parser = argparse.ArgumentParser(description="setting up prophane's databases")
    parser.add_argument('-n', action="store_true", help='dry run (no action)')
    parser.add_argument('--reverse', action="store_true", help='if you have to move the database folder to an different location, rum FIRST this script using the reverse option, then move the directory, then run this script on the new location again without this option.')
    return parser.parse_args()

def main():
    args = parse_args()
    set_dir(test=args.n, reverse = args.reverse)
    if args.n:
        print("test mode: no actions executed")
    else:
        print("all done")

if __name__ == "__main__":
    main()
