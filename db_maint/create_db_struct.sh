DATE=`date +%Y%m%d`
DATE=20181018
TEMPLATE_DIR=/home/hschieb/development/prophane/templates/dbs
TEMPLATE_DATE=20180808

# TODO
# assert prophane env is installed

# ncbi nr
F=nr.gz
REL_DIR=nr
DIR="$REL_DIR/$DATE"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/$TEMPLATE_DATE/$REL_DIR.yaml"
mkdir -p "$DIR"
mv "$F" "$DIR"
mv "$F.md5" "$DIR"
gunzip -vk "$DIR/$F"
cp "$YAML_SRC" "$DIR/"

# uniprot swissprot
F=uniprot_sprot.fasta.gz
REL_DIR=swissprot
DIR="$REL_DIR/$DATE"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/$TEMPLATE_DATE/uniprot_sprot.yaml"
mkdir -p "$DIR"
mv "$F" "$DIR"
# mv "$F.md5" "$DIR"
gunzip -vk "$DIR/$F"
cp "$YAML_SRC" "$DIR/"

# uniprot trembl
F=uniprot_trembl.fasta.gz
REL_DIR=trembl
DIR="$REL_DIR/$DATE"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/$TEMPLATE_DATE/uniprot_trembl.yaml"
mkdir -p "$DIR"
mv "$F" "$DIR"
# mv "$F.md5" "$DIR"
gunzip -vk "$DIR/$F"
cp "$YAML_SRC" "$DIR/"

# acc2tax
F1=prot.accession2taxid.gz
F2=pdb.accession2taxid.gz
REL_DIR=prot2taxid
DIR="$REL_DIR/$DATE"
# YAML_SRC="$TEMPLATE_DIR/$REL_DIR/$TEMPLATE_DATE/$REL_DIR.yaml"
mkdir -p "$DIR"
mv "$F1" "$DIR"
mv "$F1.md5" "$DIR"
mv "$F2" "$DIR"
mv "$F2.md5" "$DIR"
# gunzip -vk "$DIR/$F"
# cp "$YAML_SRC" "$DIR/"

# taxdump
F=taxdump.tar.gz
REL_DIR=taxdump
DIR="$REL_DIR/$DATE"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/$TEMPLATE_DATE/$REL_DIR.yaml"
mkdir -p "$DIR"
mv "$F" "$DIR"
mv "$F.md5" "$DIR"
# gunzip -vk "$DIR/$F"
# tar -xzvf "$DIR/$F" -C "$DIR" && rm "$DIR/$F"
cp "$YAML_SRC" "$DIR/"

# uniprot_tax
F1='index.html?sort=&desc=&compress=no&query=&fil=&force=no&preview=true&format=tab&columns=id'
F2=idmapping.dat.gz
REL_DIR=uniprot_tax
DIR="$REL_DIR/$DATE"
# YAML_SRC="$TEMPLATE_DIR/$REL_DIR/$TEMPLATE_DATE/$REL_DIR.yaml"
mkdir -p "$DIR"
mv "$F1" "$DIR/uniprot_tax.txt"
mv "$F2" "$DIR"
# gunzip -vk "$DIR/$F"
# cp "$YAML_SRC" "$DIR/"

# tigrfams
F=TIGR*
V=15
REL_DIR=tigrfams
DIR="$REL_DIR/$V"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/15/TIGRFAMs_15.0_HMM.yaml"
mkdir -p "$DIR"
mv -t "$DIR" $F
# mv "$F.md5" "$DIR"
# gunzip -vk "$DIR/$F"
cp "$YAML_SRC" "$DIR/"

# pfams
F1=Pfam-A.hmm.gz
F2=Pfam-A.clans.tsv.gz
F3=clan.txt.gz
V=15
REL_DIR=pfams
DIR="$REL_DIR/$V"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/31/Pfam-A.yaml"
mkdir -p "$DIR"
mv -t "$DIR" "$F1" "$F2" "$F3"
# mv "$F.md5" "$DIR"
gunzip -vk "$DIR/$F1"
cp "$YAML_SRC" "$DIR/"

# eggnog
F1=eggnog4.functional_categories.txt
F2=all_OG_annotations.tsv.gz
V=4_5_1
REL_DIR=eggnog
DIR="$REL_DIR/$V"
YAML_SRC="$TEMPLATE_DIR/$REL_DIR/4_5_1/${REL_DIR}_4_5_1.yaml"
mkdir -p "$DIR"
mv -t "$DIR" "$F1" "$F2"
# mv "$F.md5" "$DIR"
# gunzip -vk "$DIR/$F"
cp "$YAML_SRC" "$DIR/"
# download actual database
mkdir -p "$DIR/data"
source activate prophane
download_eggnog_data.py -y -f --data_dir "$DIR/data" euk bact arch viruses
source deactivate

# skip
cp -r "$TEMPLATE_DIR/skip" skip
