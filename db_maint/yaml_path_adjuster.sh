DB_BASE_PATH=$1

function print_help()
{
    echo Script to adjust the paths in the yamls within the given path.
    echo USAGE: yaml_renamer.sh ABSOLUTE_BASE_PATH_OF_DB_FOLDER
}

# execution with no arguments -> print help
if [[ $# -eq 0 ]] ; then
    print_help
    exit 0
fi

if ! [ -e $DB_BASE_PATH ]; then
    echo "provided path does not exitst: $DB_BASE_PATH"
    echo "printing help:"
    print_help
    exit 1
elif ! [[ $DB_BASE_PATH == /* ]]
then
    echo "provided path must be absolute"
    exit 1
else
    echo "changing yaml paths to provided path: $DB_BASE_PATH"
fi

LST_YAMLS=$(find $DB_BASE_PATH -name *.yaml)

for FILE in $LST_YAMLS
do
    sed -i "s|/media/stephan/Elements/prophane/|$DB_BASE_PATH|g" $FILE
done
