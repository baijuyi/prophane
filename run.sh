#!/usr/bin/env bash

# set bash mode to strict
set -euo pipefail
IFS=$'\n\t'

# Absolute path this script is in.
PROPHANE_DIR=$(dirname $(readlink -f $0))
FIRST_ARG=${1:-}
CONDA_ENV=${CONDA_ENV:-prophane}

# print to stderr instead of stdout
errcho(){ >&2 echo $@; }

display_help() {
    echo "Prophane Pipeline (powered by Snakemake)"
	echo ""
	echo "Usage: $0 CONFIG_FILE [Snakemake options]"
	echo
	echo " Full list of parameters:"
    echo "   --help                 show Snakemake help (or snakemake -h)"
    echo "   --list-dbs             print list of configured databases"
    echo "                          databases are looked up in 'db_base_dir' configured in:"
    echo "                              ${PROPHANE_DIR}/general_config.yaml"
    echo "   --list-styles          print list of available input file styles"
    echo "                          styles are looked up in the following folder:"
    echo "                              ${PROPHANE_DIR}/styles"
    echo "   --db-maintenance       trigger database maintenance scripts"
    echo "                          will migrate database structure from deprecated structure to most recent"
	echo
    echo " Useful Snakemake parameters:"
	echo "   -j, --cores            number of cores"
	echo "   -k, --keep-going       go on with independent jobs if a job fails"
	echo "   -n, --dryrun           do not execute anything"
	echo "   -p, --printshellcmds   print out the shell commands that will be executed"
	echo "   -t, --timestamp  		add a timestamp to all logging output"
    echo
}

# exit if specified CONDA_ENV is not present
if ! [[ $(conda env list | grep -E "^${CONDA_ENV}\s") ]]; then
    echo "Conda environment '$CONDA_ENV' does not exist. Did you execute setup.sh?"
    exit 1
fi

errcho "activating conda env: ${CONDA_ENV}"
source $(conda info --root)/etc/profile.d/conda.sh
set +u; conda activate ${CONDA_ENV}; set -u

if [ "$FIRST_ARG" == "--help" ]; then
    snakemake --help
    conda deactivate
    exit 0
elif [ "$FIRST_ARG" == "" -o "$FIRST_ARG" == "-h" ]; then
    display_help
    conda deactivate
    exit 0
elif [ "$FIRST_ARG" == "--list-dbs" ]; then
    ${PROPHANE_DIR}/database_cli.py --list_dbs
    conda deactivate
    exit 0
elif [ "$FIRST_ARG" == "--list-styles" ]; then
    ${PROPHANE_DIR}/print_style_summary.py
    conda deactivate
    exit 0
elif [ "$FIRST_ARG" == "--db-maintenance" ]; then
    ${PROPHANE_DIR}/database_cli.py --migrate_dbs
    conda deactivate
    exit 0
fi

# is first argument a file?
if ! [ -f $FIRST_ARG ]; then
    echo "ERROR"
    echo "${FIRST_ARG}: No such file or directory"
    display_help
    conda deactivate
    exit 1
fi

# prophane execution
errcho "$(date +%Y%m%d-%H:%M:%S) - starting"
snakemake --snakefile ${PROPHANE_DIR}/Snakefile --use-conda --configfile "$@"
EXT_CD=$?
conda deactivate
errcho "$(date +%Y%m%d-%H:%M:%S) - all done"

exit "$EXT_CD"
