rule report_mafft:
  input:
    'algn/pg.{n}.mafft'
  output:
    'algn/mafft.{n}.txt'
  message:
    "checking pairwise identity..."
  run:
    #read seqs
    seqs = []
    with open(input[0], "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            if line[0] == ">":
                seqs.append([])
            else:
                seqs[-1].append(line)
    seqs = [''.join(x) for x in seqs]
    seqlens = [len(x) for x in seqs]

    #calc relative pairwise identities
    idents = []
    for pair in list(itertools.combinations(seqs, 2)):
        l = len(pair[0])
        i = sum(1 for a, b in zip(pair[0], pair[1]) if a == b)
        idents.append(round(i/l*100,2))

    #if not seqlens:
    #    seqlens = [0]
    #if not idents:
    #    idents = [0]

    #write out
    with open(output[0], "w") as handle:
        handle.write("#min_seqlen\tmax_seqlen\tmin_rel_pairw_ident\t#max_rel_pairw_ident\n")
        handle.write(str(min(seqlens)) + "\t" + str(max(seqlens)) + "\t" + str(min(idents)) + "\t" + str(max(idents)))
