import os
import re
import yaml

from utils.helper_funcs import get_hmm_exts


def get_preprocessed_dbfiles(wildcards):
    db_files = expand(get_prog_db_string(wildcards) + "_hmmer-" + wildcards.vers + ".{ext}", ext=get_hmm_exts())
    return db_files


def get_prog_db_string(wildcards):
    task = config['tasks'][int(wildcards.taskid)]
    db_obj = get_db_object_for_task(task)
    prog_db_string = db_obj.get_hmmlib_file()
    return prog_db_string


rule build_run_hmmer_command:
  input:
    query_fasta='seqs/all.faa',
    db_yaml=lambda w: get_db_yaml_for_task(config['tasks'][int(w.taskid)]),
    hmm_lib=get_prog_db_string,
    preprocessed_dbfiles=get_preprocessed_dbfiles
  output:
    cmd_file='tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt',
    yaml='tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.yaml'
  version:
    "0.1"
  log:
    'tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.log'
  threads:
    32
  run:
    """
    Build the command for annotation of provided query file and write it to text file.
    Write out parameters to task specific yaml file.
    """
    task_no = int(wildcards.taskid)
    task = config['tasks'][task_no]
    query_file = input['query_fasta']
    prog_db_string = input.hmm_lib + "_hmmer-" + wildcards.vers
    annot_result = re.sub(r"-cmd\.txt$", "", output.cmd_file)
    log_file = log[0]

    #no params
    if 'params' not in task:
        task['params'] = {}

    #empty query
    if os.path.getsize(query_file) == 0:
        for o in output:
            open(o, "w").close()
        cmd = ""

    #hmmer
    else:
        single_args = {'Z', 'T', 'E'}
        cmd = f"{task['prog']} --noali --notextw --cpu {threads} --tblout {annot_result} -o {log_file} "
        cmd += " ".join([f"--{x[0]} {x[1]}" if x[0] not in single_args else f"-{x[0]} {x[1]}" for x in task['params'].items()])
        cmd += f" {prog_db_string} {query_file}"

    # write cmd_file
    with open(output.cmd_file, "w") as handle:
        handle.write(cmd)
        handle.write("\n")

    #write yaml
    with open(output.yaml, "w") as handle:
        task = dict(task)
        if params in task:
            task['params'] = dict(task['params'])
        handle.write(yaml.dump(task))


rule run_hmmer:
  input:
    cmd_file='tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt'
  output:
    'tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.result'
  message:
    "performing task {wildcards.taskid} using hmmer ..."
  conda:
    "../envs/hmmer-{vers}.yaml"
  log:
    'tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.log'
  benchmark:
    "tasks/{annot_type}_annot_by_hmmer-{vers}_on_{db_type}.task{taskid}.{md5sum}.result.benchmark.txt"
  resources:
    mem_mb=800
  version:
    "0.1"
  threads:
    32
  shell:
    """
    bash {input.cmd_file}
    """
