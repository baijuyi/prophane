10239	9VIRU	Viruses			Vira; Viridae		Superkingdom			
10486		Iridoviridae					Family	Viruses; dsDNA viruses, no RNA stage	35237	
10487		Iridovirus	small iridescent insect viruses				Genus	Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Betairidovirinae	2017757	
10491		Chloriridovirus					Genus	Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Betairidovirinae	2017757	
10492		Ranavirus					Genus	Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Alphairidovirinae	2017756	
10493	FRG3V	Frog virus 3	FV-3		FV3; Frog virus 3 iridovirus; frog virus 3 FV3; frog virus 3, FV3	annotated	Species	Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Alphairidovirinae; Ranavirus	10492	Lithobates pipiens (Northern leopard frog) (Rana pipiens)
1126234		Salinirubrum litoreum			"Salinarubrum litoreum" Cui and Qui 2014; CGMCC 1.12237; Halobacteriaceae archaeon XD46; Halobacteriaceae archaeon YJ-63-S1; Halobacteriaceae archaeon ZS-1-H; JCM 18649; Salinarubrum litoreum; Salinirubrum litoreum corrig. Cui and Qiu 2014; strain XD46	annotated	Species	Archaea; Euryarchaeota; Halobacteria; Halobacteriales; Halobacteriaceae; Salinirubrum	1644062	
131567		cellular organisms			biota					
1392996		Candidatus Methanoperedenaceae			"Candidatus Methanoperedenaceae" Haroon et al. 2013; Methanoperedenaceae		Family	Archaea; Euryarchaeota; Methanomicrobia; Methanosarcinales	94695	
1392997		Candidatus Methanoperedens			"Candidatus Methanoperedens" Haroon et al. 2013; Methanoperedens		Genus	Archaea; Euryarchaeota; Methanomicrobia; Methanosarcinales; Candidatus Methanoperedenaceae	1392996	
1462422		Candidatus Parvarchaeota			Parvarchaeota; Parvarchaeota Rinke et al. 2013		Phylum	Archaea	1783276	
1644062		Salinirubrum			"Salinarubrum" Cui and Qiu 2013; Salinarubrum; Salinirubrum corrig. Cui and Qiu 2014		Genus	Archaea; Euryarchaeota; Halobacteria; Halobacteriales; Halobacteriaceae	2236	
176652	IIV6	Invertebrate iridescent virus 6	IIV-6	Chilo iridescent virus	CIV; Chilo iridescent virus CIV; Chilo iridescent virus type 6; Iridescent virus type 6; Iridovirus CIV; insect iridescent virus type 6	reviewed	Species	Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Betairidovirinae; Iridovirus	10487	Acheta domesticus (House cricket);Gryllus bimaculatus (Two-spotted cricket);Spodoptera frugiperda (Fall armyworm);Gryllus campestris;Chilo suppressalis (Asiatic rice borer moth)
1783276		DPANN group						Archaea	2157	
1821054		environmental samples						Archaea; Euryarchaeota; Methanomicrobia; Methanosarcinales; Candidatus Methanoperedenaceae; Candidatus Methanoperedens	1392997	
1821055		uncultured Candidatus Methanoperedens sp.				annotated	Species	Archaea; Euryarchaeota; Methanomicrobia; Methanosarcinales; Candidatus Methanoperedenaceae; Candidatus Methanoperedens; environmental samples	1821054	
183963		Halobacteria			"Halomebacteria" Cavalier-Smith 1986; Halobacteria Grant et al. 2002 emend. Gupta et al. 2015; Halomebacteria; Halomebacteria Cavalier-Smith 2002		Class	Archaea; Euryarchaeota	28890	
2017756		Alphairidovirinae					Subfamily	Viruses; dsDNA viruses, no RNA stage; Iridoviridae	10486	
2017757		Betairidovirinae					Subfamily	Viruses; dsDNA viruses, no RNA stage; Iridoviridae	10486	
2157	9ARCH	Archaea			"Archaea" Woese et al. 1990; "Archaebacteria" (sic) Woese and Fox 1977; Archaebacteria; Mendosicutes; Metabacteria; Monera; Procaryotae; Prokaryota; Prokaryotae; prokaryote; prokaryotes		Superkingdom		131567	
2235		Halobacteriales			"Haloarchaeales" DasSarma and DasSarma 2008; Haloarchaeales; Halobacteriales Grant and Larsen 1989 emend. Gupta et al. 2015		Order	Archaea; Euryarchaeota; Halobacteria	183963	
2236		Halobacteriaceae			"Haloarchaeaceae" DasSarma and DasSarma 2008; Haloarchaeaceae; Halobacteriaceae Gibbons 1974 (Approved Lists 1980) emend. Gupta et al. 2016; Halobacteriaceae Gibbons 1974 emend. Gupta et al. 2015		Family	Archaea; Euryarchaeota; Halobacteria; Halobacteriales	2235	
224756		Methanomicrobia			'Methanomicrobia'		Class	Archaea; Euryarchaeota	28890	
28890	9EURY	Euryarchaeota			"Euryarchaeota" Woese et al. 1990; Euryarchaeota Garrity and Holt 2002; Methanobacteraeota; Methanobacteraeota Oren et al. 2015; euryarchaeotes; not Euryarchaeota Cavalier-Smith 2002		Phylum	Archaea	2157	
345201	IIV3	Invertebrate iridescent virus 3	IIV-3	Mosquito iridescent virus		reviewed	Species	Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Betairidovirinae; Chloriridovirus	10491	Aedes vexans (Inland floodwater mosquito) (Culex vexans);Psorophora ferox;Culex territans;Ochlerotatus sollicitans (eastern saltmarsh mosquito);Ochlerotatus taeniorhynchus (Black salt marsh mosquito) (Aedes taeniorhynchus);Culiseta annulata
35237		dsDNA viruses, no RNA stage			dsDNA viruses			Viruses	10239	
418404		uncultured ammonia-oxidizing archaeon				annotated	Species	Archaea; environmental samples	48510	
48510		environmental samples			Archaea environmental samples			Archaea	2157	
654924	FRG3G	Frog virus 3 (isolate Goorha)	FV-3			reviewed		Viruses; dsDNA viruses, no RNA stage; Iridoviridae; Alphairidovirinae; Ranavirus	10493	Ambystoma (mole salamanders);Notophthalmus viridescens (Eastern newt) (Triturus viridescens);Lithobates pipiens (Northern leopard frog) (Rana pipiens);Dryophytes versicolor (chameleon treefrog);Rana sylvatica (Wood frog)
662758		Candidatus Parvarchaeum			"Candidatus Parvarchaeum" Baker et al. 2010; Parvarchaeum		Genus	Archaea; Candidatus Parvarchaeota	1462422	
662759	PARA4	Parvarchaeum acidiphilum			"Candidatus Parvarchaeum acidiphilum" Baker et al. 2010; Candidatus Parvarchaeum acidiphilum		Species	Archaea; Candidatus Parvarchaeota; Candidatus Parvarchaeum	662758	
662760		Candidatus Parvarchaeum acidiphilum ARMAN-4				annotated		Archaea; Candidatus Parvarchaeota; Candidatus Parvarchaeum	662759	
94695		Methanosarcinales			"Halomebacteria" Cavalier-Smith 1986; Halomebacteria; Halomebacteria Cavalier-Smith 2002; Methanosarcinales Boone et al. 2002		Order	Archaea; Euryarchaeota; Methanomicrobia	224756	
12039	BYDVN	Barley yellow dwarf virus (isolate NY-RPV)	BYDV		Barley yellow dwarf virus (isolate NY-RPV) (BYDV)	reviewed		Viruses; ssRNA viruses; ssRNA positive-strand viruses, no DNA stage; Luteoviridae; Luteovirus; unclassified Luteovirus	12037	Avena sativa (Oat);Hordeum vulgare (Barley);Lolium multiflorum (Italian ryegrass) (Lolium perenne subsp. multiflorum);Lolium perenne (Perennial ryegrass);Oryza sativa (Rice);Secale cereale (Rye);Triticum aestivum (Wheat);Zea mays (Maize);Avena byzantina
10239	9VIRU	Viruses			Vira; Viridae		Superkingdom			
10237	MONMI	Moniliformis moniliformis	Thorny-headed worm	Echinorhynchus moniliformis	Moniliformis moliniformis	annotated	Species	Eukaryota; Metazoa; Lophotrochozoa; Acanthocephala; Archiacanthocephala; Moniliformida; Moniliformidae; Moniliformis	10236	
10236		Moniliformis					Genus	Eukaryota; Metazoa; Lophotrochozoa; Acanthocephala; Archiacanthocephala; Moniliformida; Moniliformidae	10235	
12037		Barley yellow dwarf virus			BYDV; Barley yellow dwarf; barley yellow dwarf virus BYDV; barley yellow dwarf virus, BYDV	annotated	Species	Viruses; ssRNA viruses; ssRNA positive-strand viruses, no DNA stage; Luteoviridae; Luteovirus; unclassified Luteovirus	368617	
10235		Moniliformidae					Family	Eukaryota; Metazoa; Lophotrochozoa; Acanthocephala; Archiacanthocephala; Moniliformida	10234	
368617		unclassified Luteovirus						Viruses; ssRNA viruses; ssRNA positive-strand viruses, no DNA stage; Luteoviridae; Luteovirus	12036	
10234		Moniliformida					Order	Eukaryota; Metazoa; Lophotrochozoa; Acanthocephala; Archiacanthocephala	10233	
12036		Luteovirus			Luteovirus subgroup I		Genus	Viruses; ssRNA viruses; ssRNA positive-strand viruses, no DNA stage; Luteoviridae	119163	
10233		Archiacanthocephala					Class	Eukaryota; Metazoa; Lophotrochozoa; Acanthocephala	10232	
119163	9LUTE	Luteoviridae					Family	Viruses; ssRNA viruses; ssRNA positive-strand viruses, no DNA stage	35278	
10232		Acanthocephala	thorny-headed worms		Acanthocephala Laporte, 1833; acanthocephalans; spiny-headed worms		Phylum	Eukaryota; Metazoa; Lophotrochozoa	1206795	
35278		ssRNA positive-strand viruses, no DNA stage						Viruses; ssRNA viruses	439488	
1206795		Lophotrochozoa						Eukaryota; Metazoa	33317	
439488		ssRNA viruses						Viruses	10239	
33317		Protostomia						Eukaryota; Metazoa	33213	
33317		Protostomia						Eukaryota; Metazoa	33213	
33213	9BILA	Bilateria						Eukaryota; Metazoa	6072	
6027		Onychodromus					Genus	Eukaryota; Alveolata; Ciliophora; Intramacronucleata; Spirotrichea; Stichotrichia; Stichotrichida; Amphisiellidae	99916	
6072		Eumetazoa						Eukaryota; Metazoa	33208	
6072		Eumetazoa						Eukaryota; Metazoa	33208	
33208	9METZ	Metazoa			Animalia; animals; metazoans; multicellular animals		Kingdom	Eukaryota	33154	
99916		Amphisiellidae					Family	Eukaryota; Alveolata; Ciliophora; Intramacronucleata; Spirotrichea; Stichotrichia; Stichotrichida	33832	
33154		Opisthokonta			Fungi/Metazoa group; Opisthokonta Cavalier-Smith 1987; opisthokonts			Eukaryota	2759	
33832	9STIC	Stichotrichida					Order	Eukaryota; Alveolata; Ciliophora; Intramacronucleata; Spirotrichea; Stichotrichia	194286	
194286		Stichotrichia			Stichotrichia Small & Lynn, 1985		Subclass	Eukaryota; Alveolata; Ciliophora; Intramacronucleata; Spirotrichea	33829	
2759	9EUKA	Eukaryota			Eucarya; Eucaryotae; Eukarya; Eukaryotae; eucaryotes; eukaryotes		Superkingdom		131567	
194286		Stichotrichia			Stichotrichia Small & Lynn, 1985		Subclass	Eukaryota; Alveolata; Ciliophora; Intramacronucleata; Spirotrichea	33829	
2759	9EUKA	Eukaryota			Eucarya; Eucaryotae; Eukarya; Eukaryotae; eucaryotes; eukaryotes		Superkingdom		131567	
194286		Stichotrichia			Stichotrichia Small & Lynn, 1985		Subclass	Eukaryota; Alveolata; Ciliophora; Intramacronucleata; Spirotrichea	33829	
2759	9EUKA	Eukaryota			Eucarya; Eucaryotae; Eukarya; Eukaryotae; eucaryotes; eukaryotes		Superkingdom		131567	
33829	9SPIT	Spirotrichea			Spirotricha; Spirotrichea Buetschli, 1889; Spirotrichia; Spirotrichs		Class	Eukaryota; Alveolata; Ciliophora; Intramacronucleata	431838	
431838		Intramacronucleata			Intramacronucleata Lynn, 1996		Subphylum	Eukaryota; Alveolata; Ciliophora	5878	
5878	9CILI	Ciliophora			Ciliata; ciliates			Eukaryota; Alveolata	33630	
33630	9ALVE	Alveolata			alveolates			Eukaryota	2759	
