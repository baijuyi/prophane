#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from utils.style_handling import StyleAccessor
from utils.input_output import load_yaml
import sys


def main():
    style_path = os.path.dirname(__file__) + "/styles"
    try:
        styleaccessor = StyleAccessor(style_path)
    except ValueError as e:
        print(str(e))
        sys.exit(1)
    styleaccessor.print_summary()
    pass


if __name__ == "__main__":
    main()
