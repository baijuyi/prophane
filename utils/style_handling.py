#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from utils.input_output import load_yaml
import argparse
import glob
import os
import pandas as pd
from pkg_resources import parse_version


class Style(object):
    def __init__(self, yaml):
        self.yaml = yaml
        self._config = self._load_yaml()

    @staticmethod
    def is_style_yaml(yaml):
        required_keys = ['style', 'program', 'version', 'comment']
        yaml_dict = load_yaml(yaml)
        if type(yaml_dict) is not dict:
            return False
        if all(key in yaml_dict for key in required_keys):
            return True
        else:
            return False

    def _load_yaml(self):
        if self.is_style_yaml(self.yaml):
            return load_yaml(self.yaml)
        else:
            raise ValueError("Not a valid style yaml: {}".format(self.yaml))

    def get_program(self):
        return self._config['program'].__str__()

    def get_version(self):
        return self._config['version'].__str__()

    def get_comment(self):
        return self._config['comment'].__str__()

    def get_yaml(self):
        return self.yaml


class StyleAccessor(object):
    def __init__(self, style_path):
        self._style_path = style_path
        self._style_yamls = self._get_style_yamls()
        self._df_style_infos = self._get_style_infos()

    def _get_style_yamls(self):
        style_yamls = [f for f in glob.glob(self._style_path + "/*.yaml") if Style.is_style_yaml(f)]
        if not style_yamls:
            raise ValueError('No valid styles were found in \n\t{}'.format(self._style_path))
        return style_yamls

    def _get_style_infos(self):
        df = pd.DataFrame()
        for f in self._style_yamls:
            s = pd.Series()
            style = Style(f)
            s['source'] = style.get_program()
            s['version'] = style.get_version()
            s['comment'] = style.get_comment()
            s['report_style'] = style.get_yaml()
            df = df.append(s, ignore_index=True)
        return df

    def get_style_yaml(self, source, version):
        df = self._df_style_infos
        df = df[df['source'] == source]
        yaml = df[df['version'] == version]['report_style'].values[0]
        return yaml

    def print_summary(self):
        print('\n')
        print('Styles available in "{}":\n'.format(self._style_path))
        df = self._df_style_infos.sort_values(by=['source', 'version'])
        df['report_style'] = df['report_style'].apply(lambda x: os.path.abspath(x), self._style_path)
        with pd.option_context('display.max_colwidth', -1):
            print(df[['source', 'version', 'report_style']].to_string(index=False, justify='left'))
        pass
