from utils.db_handling.databases.hmm_required_classes import HmmDatabase


class DbcanDatabase(HmmDatabase):
    name = "dbcan"
    valid_types = [name]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super(DbcanDatabase, self).__init__(*args, **kwargs)
