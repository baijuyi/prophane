from .helper_classes import DbYamlInteractor, Database
from utils.exceptions import DatabaseError
from .eggnog import EggnogDatabase
from .ncbi import NcbiNrDatabase
from .ncbi_uniprot_required_classes import NcbiTaxdumpDatabase, NcbiAcc2TaxDatabase
from .pfams import PfamsDatabase
from .tigrfams import TigrfamsDatabase
from .foam import FoamDatabase
from .resfams import ResfamsDatabase
from .dbcan import DbcanDatabase
from .uniprot import UniprotDatabase, UniprotTaxDatabase

LST_AVAILABLE_DBS = [
    EggnogDatabase,
    NcbiNrDatabase,
    NcbiTaxdumpDatabase,
    NcbiAcc2TaxDatabase,
    PfamsDatabase,
    TigrfamsDatabase,
    FoamDatabase,
    ResfamsDatabase,
    DbcanDatabase,
    UniprotDatabase,
    UniprotTaxDatabase
]


class Databases:
    def __init__(self, yamls):
        self._db_obj_dict = self._build_db_obj_dict(yamls)

    def __iter__(self):
        return iter(list(self._db_obj_dict.values()))

    def _build_db_cfg_dict(self, yamls):
        cfg_dct = {}
        for yaml in yamls:
            cfg_dct[yaml] = DbYamlInteractor.get_db_config_dict_from_yaml(yaml)
        return cfg_dct

    def _build_db_obj_dict(self, yamls):
        cfg_dct = self._build_db_cfg_dict(yamls)
        dct_db_objs = {}
        dct_delayed_db_classes_and_cfgs = {}
        for yaml_path, cfg_dct in cfg_dct.items():
            db_type = Database.get_type_from_db_config_dict(cfg_dct)
            db_cls = self.get_db_class_for_type(db_type)
            if db_cls.is_requiring_helper_dbs():
                dct_delayed_db_classes_and_cfgs[yaml_path] = (db_cls, cfg_dct)
            else:
                dct_db_objs[yaml_path] = db_cls(yaml_path, db_yaml_dict=cfg_dct)
        for yaml, (cls, cfg) in dct_delayed_db_classes_and_cfgs.items():
            lst_of_tuples_of_required_dbs = cls.get_lst_of_tuples_of_required_dbs(cfg)
            lst_required_db_objs = []
            for helper_db_type, helper_db_version in lst_of_tuples_of_required_dbs:
                required_db_obj = self.get_database_obj_for_type_and_version(
                    db_type=helper_db_type, version=helper_db_version, dct_db_objs=dct_db_objs)
                lst_required_db_objs.append(required_db_obj)
            dct_db_objs[yaml] = cls(yaml=yaml, db_yaml_dict=cfg, required_dbs=lst_required_db_objs)
        return dct_db_objs

    @staticmethod
    def get_db_class_for_type(db_type):
        lst_fitting_db_classes = []
        for db_cls in LST_AVAILABLE_DBS:
            if db_cls.is_valid_type(db_type):
                lst_fitting_db_classes.append(db_cls)
        if len(lst_fitting_db_classes) == 1:
            db_cls_accepting_db_type = lst_fitting_db_classes[0]
        elif len(lst_fitting_db_classes) > 1:
            cls_names = [cls.name for cls in lst_fitting_db_classes]
            raise ValueError(
                f"Multiple classes accept db_type '{db_type}'.\n"
                + "Accepted type of each db class must be unique. Non-unique classes:\n"
                + "\n".join(cls_names)
            )
        else:  # len(lst_fitting_db_classes) == 0
            raise DatabaseError(
                f"No database class accepts db_type: '{db_type}'\n"
                + "valid types are:\n"
                + "\n".join([t for db_cls in LST_AVAILABLE_DBS for t in db_cls.valid_types])
            )
        return db_cls_accepting_db_type

    def get_database_obj_for_type_and_version(self, db_type, version, dct_db_objs=None):
        if not dct_db_objs:
            dct_db_objs = self._db_obj_dict
        lst_fitting_objs = []
        for db_o in dct_db_objs.values():
            if (version == db_o.get_version()
                    and db_type == db_o.get_type()):
                lst_fitting_objs.append(db_o)
        if len(lst_fitting_objs) < 1:
            raise DatabaseError("No Database can satisfy demands:\n"
                                + f"db_type: {db_type}\n"
                                + f"db_version: {version}\n")
        elif len(lst_fitting_objs) > 1:
            raise DatabaseError("More than one Database can satisfy demands:\n"
                                + f"db_type: {db_type}\n"
                                + f"db_version: {version}\n"
                                + "\n".join(lst_fitting_objs) + "\n")
        else:
            db_obj = lst_fitting_objs[0]
        return db_obj

    def get_db_objects(self):
        return self._db_obj_dict.values()

    def get_db_obj_for_yaml(self, yaml):
        return self._db_obj_dict[yaml]
