from utils.db_handling.databases.helper_classes import Database


class EggnogDatabase(Database):
    name = "eggnog"
    valid_types = [name]
    mandatory_keys = Database.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super(EggnogDatabase, self).__init__(*args, **kwargs)

    def get_og_annotations_file(self):
        return self.join_with_db_path(self._config["files"]['og_annotations'])

    def get_fun_cats_file(self):
        return self.join_with_db_path(self._config["files"]['fun_cats'])

    def get_data_dir(self):
        return self.join_with_db_path(self._config["files"]["emapper_data"])
