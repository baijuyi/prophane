import datetime
import os
import logging
import re
from abc import ABC, abstractmethod
from utils.input_output import is_path_writable_or_can_be_created
from utils.helper_funcs import recursively_iter_dict
from utils.db_handling.databases.helper_classes import DbYamlInteractor

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Migrator(ABC):
    def __init__(self, lst_yaml_objs):
        self.lst_yaml_objs = lst_yaml_objs
        self.assert_no_db_is_below_from_version()
        self.yaml_objs_to_migrate = self.get_yamls_to_migrate()
        self.migrated_yaml_objs = self.get_yamls_already_satisfying_target_version()
        self.files = self.get_files_to_change()

    def assert_no_db_is_below_from_version(self):
        for y in self.lst_yaml_objs:
            if y.get_db_schema_version() < self.from_version:
                raise ValueError("Provided Database has schema version lower than this migrator can deal with: \n"
                                 + f"\trequired version: {self.from_version}\n"
                                 + f"\tdatabase version {y.get_db_schema_version()}\n"
                                 + f"\tdatabase: {y.get_yaml()}\n")

    def get_yamls_to_migrate(self):
        yamls_to_migrate = []
        for y in self.lst_yaml_objs:
            if y.get_db_schema_version() < self.to_version:
                yamls_to_migrate.append(y)
        return yamls_to_migrate

    def get_yamls_already_satisfying_target_version(self):
        yamls = []
        for y in self.lst_yaml_objs:
            if y.get_db_schema_version() >= self.to_version:
                yamls.append(y)
        return yamls

    def get_files_to_change(self):
        set_of_files = set()
        for method in self.list_of_ordered_migration_methods:
            set_of_files.update(method(dry_run=True))
        return set_of_files

    def migrate(self):
        protected_files = self.get_protected_files()
        if protected_files:
            raise PermissionError(
                "Write access required to the following files for database style migration.\n"
                + "Please make the following files writeable and try again:\n"
                + '\n'.join(protected_files)
            )
        for method in self.list_of_ordered_migration_methods:
            method()
        self.bump_version_of_yaml_objs_to_migrate()
        self.write_yamls_of_yaml_objs_to_migrate()
        self.migrated_yaml_objs += self.yaml_objs_to_migrate
        return self.migrated_yaml_objs

    @property
    @abstractmethod
    def from_version(self):
        return None

    @property
    @abstractmethod
    def to_version(self):
        return None

    @property
    @abstractmethod
    def list_of_ordered_migration_methods(self):
        return []

    def get_protected_files(self):
        lst_protected_files = []
        for f in set(self.files):
            if not is_path_writable_or_can_be_created(f):
                lst_protected_files.append(f)
        return lst_protected_files

    def bump_version_of_yaml_objs_to_migrate(self):
        for y in self.yaml_objs_to_migrate:
            y._config["db_schema_version"] = self.to_version

    def write_yamls_of_yaml_objs_to_migrate(self):
        for y in self.yaml_objs_to_migrate:
            y.write_yaml(force=True)


def test_fail_of_migrator_without_method_overriding():
    db = ""
    import pytest
    with pytest.raises(TypeError):
        Migrator(db)


class MigrateV00toV01(Migrator):
    """
    Remove non-gzipped map files (*.map) from file system
    adjust paths in affected yamls (*.map -> *.map.gz)
    add "db_schema_version: 1" to yamls
    """
    from_version = 0
    to_version = 1

    @property
    def list_of_ordered_migration_methods(self):
        return [
            self.remove_old_map_files,
            self.update_db_config_map_entries
        ]

    def remove_old_map_files(self, dry_run=False):
        manipulated_files = []
        for yaml_object in self.yaml_objs_to_migrate:
            db_config = yaml_object.get_config()
            map_files = self.get_all_db_associated_map_files(db_config)
            for map_file in map_files:
                if self._is_plain_map_entry(map_file):
                    if os.path.exists(map_file):
                        if not dry_run:
                            os.remove(map_file)
                        manipulated_files.append(map_file)
        return manipulated_files

    def get_all_db_associated_map_files(self, db_config):
        map_files = []
        for k, v in recursively_iter_dict(db_config):
            if self._is_map_entry(k):
                map_files.append(v)
        return map_files

    def _is_map_entry(self, config_key):
        if config_key.endswith('map'):
            return True
        else:
            return False

    def _is_plain_map_entry(self, map_file):
        if type(map_file) is str:
            if map_file.endswith('.map'):
                return True
        else:
            raise ValueError('Got element of type "{}", can only process str'.format(type(map_file)))
        return False

    def update_db_config_map_entries(self, dry_run=False):
        adjusted_files = []
        for yaml_obj in self.yaml_objs_to_migrate:
            yaml_obj_config = yaml_obj.get_config()
            map_entry_keys = [k for k in yaml_obj_config if self._is_map_entry(k)]
            if map_entry_keys:
                adjusted_files.append(yaml_obj.get_yaml())
                if not dry_run:
                    for map_key in map_entry_keys:
                        map_path = yaml_obj_config[map_key]
                        yaml_obj._config[map_key] = self.replace_map_by_map_gz(map_path)
        return adjusted_files

    @staticmethod
    def replace_map_by_map_gz(map_path):
        regex = re.compile(r".map$")
        new_map_path = regex.sub(".map.gz", map_path)
        return new_map_path


def test_replace_map_by_map_gz():
    assert MigrateV00toV01.replace_map_by_map_gz("wasd.map") == "wasd.map.gz"


class MigrateV01toV02(Migrator):
    """
    Remove skip.yaml
    bump "db_schema_version: 2" in yaml
    """
    from_version = 1
    to_version = 2

    @property
    def list_of_ordered_migration_methods(self):
        return [
            self.remove_skip_yaml
        ]

    def remove_skip_yaml(self, dry_run=False):
        adjusted_files = []
        lst_skip_yaml_objs = [y for y in self.yaml_objs_to_migrate if y.get_yaml().endswith("skip.yaml")]
        if len(lst_skip_yaml_objs) == 1:
            skip_yaml_obj = lst_skip_yaml_objs[0]
            skip_yaml_file = skip_yaml_obj.get_yaml()
            adjusted_files.append(skip_yaml_file)
            if not dry_run:
                os.remove(skip_yaml_file)
                self.yaml_objs_to_migrate = [y for y in self.yaml_objs_to_migrate
                                             if not y.get_yaml().endswith("skip.yaml")]
        elif len(lst_skip_yaml_objs) > 1:
            raise ValueError(
                "Expected one skip.yaml db config but got multiple:\n"
                + "\n".join(lst_skip_yaml_objs)
            )
        return adjusted_files


class MigrateV02toV03(Migrator):
    """
    Add accession2taxid yaml
    Add uniprot_tax yaml
    Change taxdump scope to 'helper_db'
    Add required_dbs section to nr and uniprot yamls
    Rename database folders to match db_name and db_version property
    add "fasta_gz" relative path to ncbi + uniprot yamls
    add "hmmlib" relative path to tigrfams + pfams yamls
    change absolute paths in eggnog to relative paths and move the to 'files' section in yaml
    bump "db_schema_version: 3" in yaml
    """
    from_version = 2
    to_version = 3

    @property
    def list_of_ordered_migration_methods(self):
        return [
            self.add_accession2taxid_yaml,
            self.add_uniprot_tax_yaml,
            self.change_taxdump_scope_to_helper_db,
            self.add_taxdump_tar_to_taxdump_yaml_files,
            self.add_required_dbs_to_nr_and_uniprot,
            self.add_files_section_to_eggnog,
            self.remove_non_relative_paths,
            self.rename_db_folder_to_yaml_attributes_and_adjust_yaml_path,
            self.add_unprocessed_sequence_lib_to_yaml
        ]

    def add_accession2taxid_yaml(self, dry_run=False):
        adjusted_files = []
        uniprot_ncbi_yaml_objs = self.get_yamls_objs_matching_db_types(
            ["ncbi_nr", "uniprot_complete", "uniprot_sp", "uniprot_tr"])
        set_acc2tax_files = set()
        for y in uniprot_ncbi_yaml_objs:
            y_cfg = y.get_config()
            lst_acc2tax_files = y_cfg["acc2tax"]
            set_acc2tax_files.update(lst_acc2tax_files)

        # build yaml_path
        db_dir = os.path.commonpath([os.path.dirname(f) for f in set_acc2tax_files])
        assert all(db_dir == os.path.dirname(f) for f in set_acc2tax_files), \
            ("All acc2tax db_files must be located in the same subdirectory\n\t"
             + "\n\t".join(set_acc2tax_files))
        yaml_path = os.path.join(db_dir, "accession2taxid.yaml")
        adjusted_files.append(yaml_path)

        # build yaml dict:
        db_config_dict = {}
        db_config_dict['name'] = "NCBI accession to taxid mapping database"
        db_config_dict['type'] = "accession2taxid"
        version_dir = os.path.basename(db_dir)
        version = self.convert_version_dir_to_datetime(version_dir)
        db_config_dict["version"] = version
        db_config_dict["comment"] = f"test database of the original database downloaded at {version}"
        db_config_dict["files"] = {os.path.basename(f).split(".")[0] + "2taxid": os.path.basename(f)
                                   for f in set_acc2tax_files}
        db_config_dict["scope"] = "helper_db"
        db_config_dict["db_schema_version"] = 2
        if not dry_run:
            yaml_obj = DbYamlInteractor(yaml=yaml_path, db_yaml_dict=db_config_dict)
            self.yaml_objs_to_migrate.append(yaml_obj)
            self.lst_yaml_objs.append(yaml_obj)
            for f in db_config_dict["files"].values():
                path = os.path.join(db_dir, f)
                assert os.path.exists(path), f"Required file does not exist: {path}"
        return adjusted_files

    def get_yamls_objs_matching_db_types(self, lst_of_db_types):
        matching_yaml_objs = []
        for y in self.lst_yaml_objs:
            y_cfg = y.get_config()
            db_type = y_cfg['type']
            if db_type in lst_of_db_types:
                matching_yaml_objs.append(y)
        return matching_yaml_objs

    def convert_version_dir_to_datetime(self, version_dir):
        return datetime.date(int(version_dir[:4]), int(version_dir[4:6]), int(version_dir[6:]))

    def add_uniprot_tax_yaml(self, dry_run=False):
        adjusted_files = []
        uniprot_yaml_objs = self.get_yamls_objs_matching_db_types(
            ["uniprot_complete", "uniprot_sp", "uniprot_tr"])
        set_uniprot_tax_files = set()
        full_path_file_dict = {}
        for y in uniprot_yaml_objs:
            y_cfg = y.get_config()
            dir_path = os.path.dirname(y_cfg["taxmap"])
            idmapping_path = os.path.join(dir_path, 'idmapping.dat.gz')
            full_path_file_dict['idmap'] = idmapping_path
            uniprot_tax_txt_path = os.path.join(dir_path, "uniprot_tax.txt")
            full_path_file_dict['uniprot_tax_txt'] = uniprot_tax_txt_path
            set_uniprot_tax_files.update([idmapping_path, uniprot_tax_txt_path])

        assert len(set_uniprot_tax_files) == 2, \
            ((f"Expected two uniprot_tax db_files, got: {len(set_uniprot_tax_files)}\n\t"
             + "\n\t".join(set_uniprot_tax_files)))

        # build yaml_path
        db_dir = os.path.commonpath([os.path.dirname(f) for f in set_uniprot_tax_files])
        assert all(db_dir == os.path.dirname(f) for f in set_uniprot_tax_files), \
            ("All uniprot_tax db_files must be located in the same subdirectory\n\t"
             + "\n\t".join(set_uniprot_tax_files))
        yaml_path = os.path.join(db_dir, "uniprot_tax.yaml")
        adjusted_files.append(yaml_path)

        # build yaml dict:
        db_config_dict = {}
        db_config_dict['name'] = "Uniprot accession to taxid and taxid to tax-name mapping database"
        db_config_dict['type'] = "uniprot_tax"
        version_dir = os.path.basename(db_dir)
        version = self.convert_version_dir_to_datetime(version_dir)
        db_config_dict["version"] = version
        db_config_dict["comment"] = f"test database of the original database downloaded at {version}"
        db_config_dict["files"] = {key: os.path.basename(path) for key, path in full_path_file_dict.items()}
        db_config_dict["scope"] = "helper_db"
        db_config_dict["db_schema_version"] = 2
        if not dry_run:
            yaml_obj = DbYamlInteractor(yaml=yaml_path, db_yaml_dict=db_config_dict)
            self.yaml_objs_to_migrate.append(yaml_obj)
            self.lst_yaml_objs.append(yaml_obj)
            for f in db_config_dict["files"].values():
                path = os.path.join(db_dir, f)
                assert os.path.exists(path), f"Required file does not exist: {path}"
        return adjusted_files

    def change_taxdump_scope_to_helper_db(self, dry_run=False):
        adjusted_files = []
        lst_yaml_objs = self.get_yamls_objs_matching_db_types(['taxdump'])
        assert len(lst_yaml_objs) == 1, f"Expected exactly one yaml object with type 'taxdump', got: {len(lst_yaml_objs)}"
        yaml_obj = lst_yaml_objs[0]
        if not dry_run:
            yaml_obj._config["scope"] = "helper_db"
        adjusted_files.append(yaml_obj.get_yaml())
        return adjusted_files

    def add_taxdump_tar_to_taxdump_yaml_files(self, dry_run=False):
        adjusted_files = []
        taxdump_yaml_obj = self.get_one_yaml_obj_matching_db_type('taxdump')
        taxdump_cfg = taxdump_yaml_obj.get_config()
        taxdump_file_paths = []
        for db_type in ["ncbi_nr", "uniprot_complete", "uniprot_sp", "uniprot_tr"]:
            y_obj = self.get_one_yaml_obj_matching_db_type(db_type)
            y_cfg = y_obj.get_config()
            taxdump_file_paths.append(y_cfg["taxdump"])
        set_taxdump_file_paths = set(taxdump_file_paths)
        assert len(set_taxdump_file_paths) == 1, \
            (f"Expected exactly one taxdump file, got {len(set_taxdump_file_paths)}\n"
             + "\n".join(set_taxdump_file_paths))
        if "files" in taxdump_cfg:
            files_dict = taxdump_cfg["files"]
        else:
            files_dict = {}
        db_dir = os.path.dirname(taxdump_yaml_obj.get_yaml())
        taxdump_path = taxdump_file_paths[0]
        assert os.path.samefile(os.path.dirname(taxdump_path), db_dir), \
            ("taxdump.tar.gz and taxdump.yaml must lie in same folder.\n"
             + f"taxdump.tar.gz path: {taxdump_path}\n"
             + f"taxdump.yaml path: {taxdump_yaml_obj.get_yaml()}")
        files_dict["taxdump"] = os.path.basename(taxdump_path)
        taxdump_cfg["files"] = files_dict
        if not dry_run:
            for f in taxdump_cfg["files"].values():
                path = os.path.join(db_dir, f)
                assert os.path.exists(path), f"Required file does not exist: {path}"
            taxdump_yaml_obj._config = taxdump_cfg
        adjusted_files.append(taxdump_yaml_obj.get_yaml())
        return adjusted_files

    def add_required_dbs_to_nr_and_uniprot(self, dry_run=False):
        adjusted_files = []
        lst_file_keys = ["acc2tax", "taxdump_map", "taxdump", "taxmap"]
        for db_type in ["ncbi_nr", "uniprot_complete", "uniprot_sp", "uniprot_tr"]:
            y_obj = self.get_one_yaml_obj_matching_db_type(db_type)
            if not dry_run:
                y_cfg = y_obj.get_config()
                lst_required_files = []
                for key, value in y_cfg.items():
                    if key in lst_file_keys:
                        if key == "acc2tax":
                            lst_required_files += value
                        else:
                            lst_required_files.append(value)
                set_required_folders = {os.path.dirname(f) for f in lst_required_files}
                lst_helper_yaml_objs = []
                for folder in set_required_folders:
                    lst_helper_yaml_objs += [
                        y for y in self.lst_yaml_objs
                        if os.path.samefile(folder, os.path.dirname(y.get_yaml()))
                    ]
                if len(lst_helper_yaml_objs) != len(set_required_folders):
                    raise ValueError(f"Expected {len(set_required_folders)} helper DBs, got {len(lst_helper_yaml_objs)}\n"
                                     + f"Folders:\n\t" + "\n\t".join(set_required_folders) + "\n"
                                     + f"Required DB yamls:\n\t"
                                     + "\n\t".join([y.get_yaml() for y in lst_helper_yaml_objs]) + "\n")
                required_dbs = {}
                for helper_db_yaml_obj in lst_helper_yaml_objs:
                    helper_db_cfg = helper_db_yaml_obj.get_config()
                    helper_db_type = helper_db_cfg["type"]
                    required_dbs[helper_db_type] = {}
                    required_dbs[helper_db_type]["version"] = helper_db_cfg["version"]
                    y_obj._config["required_dbs"] = required_dbs
            adjusted_files.append(y_obj.get_yaml())
        return adjusted_files

    def add_files_section_to_eggnog(self, dry_run=False):
        y_obj = self.get_one_yaml_obj_matching_db_type("eggnog")
        y_path = y_obj.get_yaml()
        adjusted_files = [y_path]
        y_cfg = y_obj.get_config()
        eggnog_dir = os.path.dirname(y_path)
        if "files" in y_cfg:
            files_dict = y_cfg["files"]
        else:
            files_dict = {}
        for key in ["data_dir", "og_annotations", "fun_cats"]:
            if key == "data_dir":
                new_key = "emapper_data"
            else:
                new_key = key
            full_file_path = y_cfg[key]
            rel_file_path = os.path.relpath(full_file_path, eggnog_dir)
            files_dict[new_key] = rel_file_path
        y_cfg["files"] = files_dict
        if not dry_run:
            y_obj._config = y_cfg
        return adjusted_files

    def remove_non_relative_paths(self, dry_run=False):
        adjusted_files = []
        keys_to_remove = ["acc2tax", "taxdump_map", "taxdump", "taxmap"]
        for db_type in ["ncbi_nr", "uniprot_complete", "uniprot_sp", "uniprot_tr"]:
            y_obj = self.get_one_yaml_obj_matching_db_type(db_type)
            y_cfg = y_obj.get_config()
            keys_in_y = [k for k in keys_to_remove if k in y_cfg]
            if keys_in_y:
                adjusted_files.append(y_obj.get_yaml())
                for k in keys_in_y:
                    del y_cfg[k]
            if not dry_run:
                y_obj._config = y_cfg
        # eggnog
        keys_to_remove = ["data_dir", "og_annotations", "fun_cats"]
        y_obj = self.get_one_yaml_obj_matching_db_type("eggnog")
        y_cfg = y_obj.get_config()
        keys_in_y = [k for k in keys_to_remove if k in y_cfg]
        if keys_in_y:
            adjusted_files.append(y_obj.get_yaml())
            for k in keys_in_y:
                del y_cfg[k]
        if not dry_run:
            y_obj._config = y_cfg
        return adjusted_files

    def get_one_yaml_obj_matching_db_type(self, db_type):
        lst_y_objs = self.get_yamls_objs_matching_db_types(db_type)
        if len(lst_y_objs) != 1:
            raise ValueError(f"Expected exactly one yaml object with type '{db_type}'\n"
                             + f", got: {len(lst_y_objs)}\n"
                             + "\n".join(lst_y_objs))
        return lst_y_objs[0]

    def rename_db_folder_to_yaml_attributes_and_adjust_yaml_path(self, dry_run=False):
        adjusted_files = []
        yaml_objs_to_migrate_wo_skip = [y for y in self.yaml_objs_to_migrate if not y.get_yaml().endswith("skip.yaml")]
        for yaml_object in yaml_objs_to_migrate_wo_skip:
            yaml_obj_config = yaml_object.get_config()
            db_version_str = str(yaml_obj_config['version'])
            db_type = yaml_obj_config['type']
            version_dir_path = os.path.dirname(yaml_object.get_yaml())
            db_type_dir_path = os.path.dirname(version_dir_path)
            db_base_dir = os.path.dirname(db_type_dir_path)
            lst_old_names_new_names = [
                (version_dir_path, os.path.join(db_type_dir_path, db_version_str)),
                (db_type_dir_path, os.path.join(db_base_dir, db_type))
            ]
            for old_name, new_name in lst_old_names_new_names:
                if not old_name == new_name:
                    if not dry_run:
                        os.rename(old_name, new_name)
                    adjusted_files += [old_name, new_name]
            new_yaml_name = os.path.join(db_base_dir, db_type, db_version_str,
                                         os.path.basename(yaml_object.get_yaml()))
            adjusted_files.append(new_yaml_name)
            if not dry_run:
                yaml_object.yaml = new_yaml_name
        return adjusted_files

    def add_unprocessed_sequence_lib_to_yaml(self, dry_run=False):
        adjusted_files = []
        for yaml_obj in self.yaml_objs_to_migrate:
            yaml_obj_config = yaml_obj.get_config()
            db_type = yaml_obj_config['type']
            db_is_uniprot_or_ncbi_based = (db_type == "ncbi_nr"
                                           or db_type in ["uniprot_complete", "uniprot_sp", "uniprot_tr"])
            yaml_path_basename = os.path.basename(yaml_obj.get_yaml())
            yaml_path_basename_wo_ext = os.path.splitext(yaml_path_basename)[0]
            if "files" in yaml_obj_config:
                files_dict = yaml_obj_config["files"]
            else:
                files_dict = {}
            if db_type in ['tigrfams', 'pfams']:
                adjusted_files.append(yaml_obj.get_yaml())

                if db_type == 'tigrfams':
                    prog_db_string = yaml_path_basename_wo_ext + ".LIB"
                elif db_type == 'pfams':
                    prog_db_string = yaml_path_basename_wo_ext + ".hmm"
                files_dict['hmm_lib'] = prog_db_string
            elif db_is_uniprot_or_ncbi_based:
                adjusted_files.append(yaml_obj.get_yaml())
                prog_db_string = yaml_path_basename_wo_ext + ".gz"
                files_dict['fasta_gz'] = prog_db_string
            # eggnog data dir is already added in add_files_section_to_eggnog
            yaml_obj_config["files"] = files_dict
            for f in yaml_obj_config["files"].values():
                path = os.path.join(os.path.dirname(yaml_obj.get_yaml()), f)
                assert os.path.exists(path), f"Required file does not exist: {path}"
            if not dry_run:
                yaml_obj._config = yaml_obj_config
                for f in yaml_obj.get_config()["files"].values():
                    path = os.path.join(os.path.dirname(yaml_obj.get_yaml()), f)
                    assert os.path.exists(path), f"Required file does not exist: {path}"
        return adjusted_files
