import os

from snakemake import utils as smk_utils
from utils.input_output import load_yaml


INCONCLUSIVE_ANNOTATIONS = {
    'unclassified': 'unclassified',
    'heterogenous': 'various'
}


def amend_config_with_defaults(
        user_config_dict,
        default_config_dict
):
    # default_cfg = load_yaml(default_config_path)
    user_config_dict = smk_utils.update_config(default_config_dict, user_config_dict)
    return user_config_dict


def validate_config_and_set_defaults(config_dict, schema="schemas/config.schema.yaml"):
    """
    Validates  config_dict with against config schema.
    Sets default values if not set.
    :param config_dict: dictionary to be validated
    :param schema: yaml or json schema
    :return:
    """
    smk_utils.validate(config_dict, schema, set_default=True)
    return config_dict


def amend_job_config_with_general_config(job_config, workflow_basedir):
    if 'use_test_general_config' in job_config:  # override general config path; used for test cases
        general_config_yaml = job_config['use_test_general_config']
    else:
        general_config_yaml = os.path.join(workflow_basedir, 'general_config.yaml')
    job_config['db_base_dir'] = load_yaml(general_config_yaml)['db_base_dir']
    return job_config


def test_validation():
    from utils.input_output import load_yaml
    test_dict = load_yaml('../tests/test_full_analysis_and_file_presence/job_dir/input/config.yaml')
    schema = "../schemas/config.schema.yaml"
    test_dict = validate_config(test_dict, schema)
    from pprint import pprint
    pprint(test_dict)
