#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import argparse

from utils.db_handling.db_access import DbAccessor
from utils.db_handling.migrate_database_schema_version import migrate_dbs_style_if_necessary
from utils.input_output import load_yaml


def parse_args():
    parser = argparse.ArgumentParser("Command line interface for interaction with Prophane databases.",
                                     epilog="'--list_dbs' and '--migrste_dbs' are mutually exclusive")
    parser.add_argument("--list_dbs", action="store_true", help="list configured databases")
    parser.add_argument("--migrate_dbs", action="store_true",
                        help="migrate database schema files of prophane to most recent version")
    parser.add_argument("--db_base_folder", type=str, help="Base folder of installed databases, for testing.")
    args = parser.parse_args()
    if args.list_dbs and args.migrate_dbs:
        parser.print_help()
        parser.exit()
    return args


def main():
    args = parse_args()
    if not args.db_base_folder:
        prophane_path = os.path.dirname(__file__)
        general_config = os.path.join(prophane_path, 'general_config.yaml')
        db_base_path = load_yaml(general_config)['db_base_dir']
    else:
        db_base_path = args.db_base_folder
    if args.list_dbs:
        try:
            dbaccessor = DbAccessor(db_base_path)
        except ValueError as e:
            print(str(e))
            sys.exit(1)
        dbaccessor.print_summary()
    elif args.migrate_dbs:
        migrate_dbs_style_if_necessary(db_base_path)
    pass


if __name__ == "__main__":
    main()
