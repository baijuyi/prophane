import datetime
import glob
import gzip
import io
import itertools
import numpy
import os
import pandas as pd
import re
import shutil
import snakemake.utils as smk_utils
import sys
import tarfile
import utils
import yaml

from Bio import SeqIO
from collections import defaultdict
from pytools.persistent_dict import PersistentDict
from utils.config import validate_config_and_set_defaults, amend_job_config_with_general_config
from utils.input_output import (
    get_accs_from_pg_yaml,
    get_headline,
    get_groups_to_align_from_pg_yaml,
    iter_file,
    load_yaml,
    read_taxmap)
from utils.db_handling.db_access import DbAccessor, get_db_object_for_dbtype_and_version
from utils.helper_funcs import (
    get_hmm_exts,
    get_timestamp)
from utils.helper_funcs_for_snakemake_workflow import (
    get_db_obj_for_task_and_dbdir,
    get_mafft_report,
    get_task_lca,
    get_task_map,
    get_task_plot_file,
    get_taxblast_result,)
from utils.search_result_parsing import ProteomicSearchResultParser
from warnings import warn

# explicitly set shell used by snakemake to bash
shell.executable("/bin/bash")

# set minimum snakemake version
smk_utils.min_version("5.4")
PROPHANE_VERSION = "3.3.1"


if not config:
    sys.exit("error: no config file submitted")

config = validate_config_and_set_defaults(
    config, schema=os.path.join(workflow.basedir, "schemas", "config.schema.yaml")
)
config = amend_job_config_with_general_config(config, workflow.basedir)

LCATASKS = list(range(len(config['tasks'])))


def trace(msg, die=False):
    '''prints a TRACE message to stdout and optionally stderr which causes program termination'''
    for m in msg.split("\n"):
        print(m)
    if die:
        sys.exit(msg)

def get_all_plots(wildcards):
    plots = []
    db_base_dir = get_db_base_dir()
    for taskid, task_dict in enumerate(config["tasks"]):
        plots.append(get_task_plot_file(taskid, task_dict, db_base_dir))
    return plots

def get_db_base_dir():
    return config['db_base_dir']

def get_db_yaml_for_task(task_config_dict, db_acc=None):
    db_base_dir = config['db_base_dir']
    if not db_acc:
        db_acc = DbAccessor(db_base_dir)
    db_obj = get_db_obj_for_task_and_dbdir(task_config_dict, db_base_dir, db_acc)
    yaml_file = db_obj.get_yaml()
    return yaml_file

def get_db_object_for_task(task_config_dict, db_acc=None):
    db_base_dir = config['db_base_dir']
    if not db_acc:
        db_acc = DbAccessor(db_base_dir)
    db_obj = get_db_obj_for_task_and_dbdir(task_config_dict, db_base_dir, db_acc)
    return db_obj


workdir: config['output_dir']

# set container dir to avoid writing to prophane bin dir
storage = PersistentDict("mystorage", container_dir=config['output_dir'])
onstart:
    print("job:", config['general']['job_name'])
    # global vars within 'onstart' are not persistent, outside of 'onstart' the variable is set multiple times
    # start time is therefore stored in a persistent dict
    storage.store("START_TIME", get_timestamp())


pg_yaml_path = os.path.join(config['output_dir'], 'pgs/protein_groups.yaml')
if not os.path.isfile(pg_yaml_path):
    # protein group yaml creation
    os.makedirs(os.path.dirname(pg_yaml_path), exist_ok=True)
    if 'sample_groups' in config:
        required_sample_descriptors = [sample_name for sample_group in config['sample_groups'].values() for sample_name in sample_group]
    else:
        required_sample_descriptors = []
    if not 'decoy_regex' in config:
        decoy_shell_arg = ''
    else:
        decoy_shell_arg = '-d \'' + config['decoy_regex'].replace("'", "\\'") + '\' '

    supported_input_file_keys = ['search_result', 'report', 'mpa_report', 'mpa_multisample', 'generic_table']
    if 'search_result' in config['input']:
        proteomic_search_result_file = config['input']['search_result']
    elif 'report' in config['input']:
        proteomic_search_result_file = config['input']['report']
    elif 'mpa_report' in config['input']:
        proteomic_search_result_file = config['input']['mpa_report']
    elif 'mpa_multisample' in config['input']:
        proteomic_search_result_file = config['input']['mpa_multisample']
    elif 'generic_table' in config['input']:
        proteomic_search_result_file = config['input']['generic_table']
    elif 'pd_xml' in config['input']:
        raise NotImplementedError("Proteome Discoverer result files are currently not supported.")
        shell(workflow.basedir + "/scripts/parse_proteome_discoverer_output.py " +
              decoy_shell_arg + config['input']['report_style'] + " " + config['input']['pd_xml'] + " " +
              config['input']['pd_psms'] + " " + pg_yaml_path)
    else:
        raise ValueError("Can not parse input section of provided config. One of the following keys is required:\n"
                         f"\t{supported_input_file_keys}"
                         "Input section of provided config:"
                         f"{config['input']}")

    protein_parser = ProteomicSearchResultParser(
            result_table=proteomic_search_result_file,
            style=config['input']['report_style'],
            out_file=pg_yaml_path,
            decoy_regex=config['decoy_regex'],
            required_sample_descriptors=required_sample_descriptors
        )
    protein_parser.write_protein_groups_yaml()


rule all:
    input:
        'summary.txt',
        get_all_plots,
        'job_info.txt'
    threads: 3

#db handling
include: "rules_db_maint/make_dmnd_db.smk"
include: "rules_db_maint/make_hmm_lib.smk"
include: "rules_db_maint/make_nr_tax2annot_map.smk"
include: "rules_db_maint/make_uniprot_acc2tax_map.smk"
include: "rules_db_maint/make_nr_acc2tax_map.smk"
include: "rules_db_maint/make_tigrfam_map.smk"
include: "rules_db_maint/make_pfam_map.smk"
include: "rules_db_maint/make_foam_map.smk"
include: "rules_db_maint/make_resfam_map.smk"
include: "rules_db_maint/make_dbcan_map.smk"
include: "rules_db_maint/make_eggnog_map.smk"

#input and data mapping handling
include: "rules/map_to_seq.smk"
include: "rules/create_pg_fasta.smk"
include: "rules/tax_by_taxmap.smk"

#analyses
include: "rules/run_mafft.smk"
include: "rules/report_mafft.smk"
include: "rules/run_diamond.smk"
include: "rules/run_emapper.smk"
include: "rules/run_hmmer.smk"
include: "rules/get_besthits.smk"
include: "rules/map_task_result.smk"
include: "rules/lca_task.smk"
include: "rules/quant_pg.smk"
include: "rules/quant_task.smk"
include: "rules/create_summary.smk"

#plotting
include: "rules/create_krona.smk"

#info
include: "rules/create_info.smk"
